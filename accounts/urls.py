from django.urls import path
from accounts.views import LoginView, LogoutView, SignupView

urlpatterns = [
    path("login/", LoginView, name="login"),
    path("logout/", LogoutView, name="logout"),
    path("signup/", SignupView, name="signup"),
]
