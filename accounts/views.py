from django.shortcuts import render, redirect
from accounts.forms import LoginForm, SignupForm
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.models import User

# ==================================================================#
# Login Functions:


def LoginView(request):
    """View Function for logging in a User"""

    if request.method == "POST":

        form = LoginForm(request.POST)

        if form.is_valid():

            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]

            user = authenticate(request, username=username, password=password)

            if user is not None:
                login(request, user)
                return redirect("home")

    else:

        form = LoginForm()

    context = {"form": form}

    return render(request, "accounts/login.html", context)


def LogoutView(request):
    """View Function for Logging out a User"""

    logout(request)
    return redirect("login")


# ==================================================================#
# Sign up Functions:


def SignupView(request):
    """View Function for Signing up, and creating a new User Object"""

    if request.method == "POST":

        form = SignupForm(request.POST)

        if form.is_valid():

            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]

            if password == password_confirmation:

                NewUser = User.objects.create_user(
                    username=username, password=password
                )

                login(request, NewUser)

                return redirect(
                    "list_projects"
                )  # the tests dont account for if I put "home" here.

            else:

                form.add_error("password", "the passwords do not match")

    else:
        form = SignupForm()

    context = {"form": form}

    return render(request, "registration/signup.html", context)
