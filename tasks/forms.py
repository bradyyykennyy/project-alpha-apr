from django.forms import ModelForm
from tasks.models import Task

# Create your views here.


class TaskCreateForm(ModelForm):
    class Meta:
        model = Task
        fields = ["name", "start_date", "due_date", "project", "assignee"]
