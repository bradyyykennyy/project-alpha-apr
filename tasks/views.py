from django.shortcuts import render, redirect
from tasks.models import Task
from tasks.forms import TaskCreateForm
from django.contrib.auth.decorators import login_required


@login_required(redirect_field_name="login")
def TaskCreateView(request):

    if request.method == "POST":

        form = TaskCreateForm(request.POST)

        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:

        form = TaskCreateForm()

    context = {"form": form}

    return render(request, "tasks/create.html", context)


@login_required(redirect_field_name="login")
def TaskListView(request):

    task_objects = Task.objects.filter(assignee=request.user)

    context = {"Tasks": task_objects}

    return render(request, "tasks/list.html", context)
