from django.forms import ModelForm
from projects.models import Project

# Create your views here.


class ProjectCreateForm(ModelForm):
    class Meta:
        model = Project
        fields = ["name", "description", "owner"]
