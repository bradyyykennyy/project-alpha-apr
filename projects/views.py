from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from projects.models import Project
from projects.forms import ProjectCreateForm

# Create your views here.


@login_required(redirect_field_name="login")
def ProjectsListView(request):

    all_projects = Project.objects.filter(owner=request.user)

    context = {"Projects": all_projects}

    return render(request, "projects/list.html", context)


@login_required(redirect_field_name="login")
def ProjectDetailView(request, id):

    specific_project = get_object_or_404(Project, id=id)

    context = {"Project": specific_project}

    return render(request, "projects/detail.html", context)


@login_required(redirect_field_name="login")
def ProjectCreateView(request):

    if request.method == "POST":
        form = ProjectCreateForm(request.POST)

        if form.is_valid():
            form.save()
            return redirect("list_projects")

    else:

        form = ProjectCreateForm()

    context = {"form": form}

    return render(request, "projects/create.html", context)
