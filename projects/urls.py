from django.urls import path
from projects.views import (
    ProjectsListView,
    ProjectDetailView,
    ProjectCreateView,
)

urlpatterns = [
    path("", ProjectsListView, name="list_projects"),
    path("<int:id>/", ProjectDetailView, name="show_project"),
    path("create/", ProjectCreateView, name="create_project"),
]
